
let animacion = document.querySelectorAll(".animacion");

function mostrarAnimacion() {
    let scrollTop = document.documentElement.scrollTop;
    for (let i = 0; i < animacion.length; i++) {

        let altura = animacion[i].offsetTop;
       
        if (altura - 400 < scrollTop) {
            animacion[i].style.opacity = 1;
            animacion[i].classList.add('animacion-subir');
        }
    }
}

window.addEventListener('scroll', mostrarAnimacion);